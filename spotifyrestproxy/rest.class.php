<?php
	include 'config.class.php';

	function refreshToken($basicauth,$refreshtoken){
		$toReturn = null;
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://accounts.spotify.com/api/token",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => "grant_type=refresh_token&refresh_token=".$refreshtoken,
			CURLOPT_HTTPHEADER => array(
				"Authorization: Basic ".$basicauth,
				"Content-Type: application/x-www-form-urlencoded",
			),
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		if($err){
			echo "cURL Error #:" . $err;
		}
		else{
			$toReturn = json_decode($response)->access_token;
		}
		return $toReturn;
	}
	function startPlayback($receivedrefreshtoken,$deviceid,$defaultplay){
		$toReturn = null;
		$ifDevice = (empty($deviceid)) ? '' : '?device_id='.$deviceid;
		$ifDefaultPlay = (empty($defaultplay)) ? '' : '{"context_uri": "spotify:'.$defaultplay.'"}';
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api.spotify.com/v1/me/player/play".$ifDevice,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "PUT",
			CURLOPT_POSTFIELDS => $ifDefaultPlay,
			CURLOPT_HTTPHEADER => array(
				"Authorization: Bearer ".$receivedrefreshtoken
			),
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);

		if ($err) { echo "cURL Error #:" . $err; }
		else { echo $response; }
	}
	function pausePlayback($receivedrefreshtoken,$deviceid){
		$ifDevice = (empty($deviceid)) ? '' : '?device_id='.$deviceid;
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api.spotify.com/v1/me/player/pause".$ifDevice,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_PUT => true,
			CURLOPT_HTTPHEADER => array(
				"Authorization: Bearer ".$receivedrefreshtoken
			),
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);

		if ($err) { echo "cURL Error #:" . $err; }
		else { echo $response; }
	}
	function setVolume($receivedrefreshtoken,$deviceid,$defaultvolume){
		if(!empty($deviceid) && !empty($defaultvolume)){
			$curl = curl_init();
			curl_setopt_array($curl, array(
				CURLOPT_URL => "https://api.spotify.com/v1/me/player/volume?volume_percent=".$defaultvolume."&device_id=".$deviceid,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_PUT => true,
				CURLOPT_HTTPHEADER => array(
					"Authorization: Bearer ".$receivedrefreshtoken
				),
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);
			curl_close($curl);

			if ($err) { echo "cURL Error #:" . $err; }
			else { echo $response; }
		}
	}
	function setShuffle($receivedrefreshtoken,$deviceid,$defaultshuffle){
		$ifDevice = (empty($deviceid)) ? '' : '&device_id='.$deviceid;
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api.spotify.com/v1/me/player/shuffle?state=".$defaultshuffle.$ifDevice,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_PUT => true,
			CURLOPT_HTTPHEADER => array(
				"Authorization: Bearer ".$receivedrefreshtoken
			),
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);

		if ($err) { echo "cURL Error #:" . $err; }
		else { echo $response; }
	}
?>
	