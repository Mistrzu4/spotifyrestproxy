<?php
	/* Dont forget about the slash at the end */
	$MyPage = 'http://example.com/spotifyrestproxy/';
	/* You can get Client ID & Client Secret here: https://developer.spotify.com/dashboard/applications */
	$ClientID = 'abcdabcdabcdabcdabcdabcdabcdabcd'; 
	$ClientSecret = 'xyzxyzxyzxyzxyzxyzxyzxyzxyzxyzxy';
	/* You can get Device ID here: https://developer.spotify.com/console/get-users-available-devices/ */
	$DeviceId = 'adsfadsfadsfadsfadsfadsfadsfadsfadsfadsf';
	$RefreshToken = 'ABCdeF1-ABCdeF1ABCdeF1ABCdeF1ABCdeF1ABCdeF1ABCd_-ABCdeF1ABCdeF1ABCdeF1ABCdeF1ABCdeF1ABCdeF1ABCdeF1_ABCde--ABCdeF1ABCdeF1ABCdeF1ABCdeF1';
	/* You can choose the default album, track or playlist: 'track:5xGHeN58fFOcORW0pRZznn' */
	$DefaultPlay = 'album:2cqzFuaX52BSIPQT3w05Vm';
	/* Default Volume works only if the Device ID is not empty */
	$DefaultVolume = 10;
	 /* If true - play random song from the playlist */
	$DefaultShuffle = 'true';
?>