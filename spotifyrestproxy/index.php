<?php
include 'config.class.php';
echo 'My configuration:<br>
<b>My Website URL for Spotify</b>: '.$MyPage.'callback.php<br>
<b>My Website URL for PLAY</b>: '.$MyPage.'start.php<br>
<b>My Website URL for STOP</b>: '.$MyPage.'stop.php<br>
<b>My Device ID</b>: '.substr($DeviceId, 0, 5).'******************************'.substr($DeviceId, strlen($DeviceId)-5, 5).'<br>
<b>My Client ID</b>: '.substr($ClientID, 0, 5).'******************************'.substr($ClientID, strlen($ClientID)-5, 5).'<br>
<b>My Client Secret</b>: '.substr($ClientSecret, 0, 5).'******************************'.substr($ClientSecret, strlen($ClientSecret)-5, 5).'<br>
<b>My Refresh Token</b>: '.substr($RefreshToken, 0, 5).'******************************'.substr($RefreshToken, strlen($RefreshToken)-5, 5).'<br>
<b>My Default Play playlist/album</b>: '.$DefaultPlay.'<br>
<b>My Default Volume</b>: '.$DefaultVolume.'<br>
<b>My Default Shuffle</b>: '.$DefaultShuffle.'<br><br>
<a href="callback.php">Generate refresh token</a><br>';
?>