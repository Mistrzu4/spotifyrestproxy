<?php
include 'config.class.php';

$basicAuth = base64_encode($ClientID.':'.$ClientSecret);
$spotiLink = 'https://accounts.spotify.com/authorize/?client_id='.$ClientID.'&response_type=code&redirect_uri='.$MyPage.'callback.php&scope=user-modify-playback-state';

echo '<a href="'.$spotiLink.'">Generate!</a><br><br>';

$accessToken = null;
if(isset($_GET["code"])){
	$accessToken = $_GET["code"];
	$curl = curl_init();
	curl_setopt_array($curl, array(
		CURLOPT_URL => "https://accounts.spotify.com/api/token",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POSTFIELDS => "redirect_uri=".$MyPage."callback.php&code=".$accessToken."&grant_type=authorization_code",
		CURLOPT_HTTPHEADER => array(
			"Authorization: Basic ".$basicAuth,
			"Content-Type: application/x-www-form-urlencoded",
		),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);
	curl_close($curl);
	if ($err) {
	echo "cURL Error #:" . $err; }
	else {
		$response = json_decode($response)->refresh_token;
		echo '<b>Your refresh token:</b><br><input type="text" name="refresh_token" value="'.$response.'" size="160"><br>Now save this token in "config.class.php" file.';
	}
}