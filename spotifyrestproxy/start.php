<?php
	include 'config.class.php';
	include 'rest.class.php';

	$BasicAuth = base64_encode($ClientID.':'.$ClientSecret);
	$ReceivedRefreshToken = refreshToken($BasicAuth,$RefreshToken);
	setShuffle($ReceivedRefreshToken,$DeviceId,$DefaultShuffle);
	setVolume($ReceivedRefreshToken,$DeviceId,$DefaultVolume);
	startPlayback($ReceivedRefreshToken,$DeviceId,$DefaultPlay);
?>
	